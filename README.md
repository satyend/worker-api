
To run this app
===============

It assumes you have node and npm installed on  machine.

Server was tested using<br> 
- node version 16.14.2<br>
- npm version 8.5.0

1. extract all the files into an empty folder
2. run the command
    - npm install
3. run one of the following commands
    - npm start
    - nodemon app.js

This will start the api server on the port 5000 on localhost

__*Note:*__ you can change the port by editing the PORT setting in the *.env* file

API endpoints and examples
==========================

Create a job
------------
> POST /jobs   

body should be of the format { jobUrl: url-spec }  

example: curl -X POST localhost:5000/jobs -H "content-type: application/json" -d '{ "jobUrl": "abc" }'

this call will return a **jobId** of the job just created

Create batch job
----------------
> POST /jobs/batchjob 
  
    body should be of the format { jobUrls: [<url-spec-list>] } 

example: curl -X POST -H 'content-type: application/json' -d '{ "jobUrls": [ "https://api.coindesk.com/v1/bpi/currentprice.json", "https://catfact.ninja/fact" ] }' localhost:5000/jobs/batchjob   
    
this call will return a array of **jobId** values for the jobs created

Get job status
--------------
> GET /jobs/jobId 

example: curl 'localhost:5000/jobs/742d24a0-254d-429c-9d1f-4557a27a511e'  
      
this call will return a string  value for the jobs status **( "pending" | "done" | "error" )**
    
Get job contents
----------------
> GET /jobs/contents/jobId  

example: curl 'localhost:5000/jobs/contents/742d24a0-254d-429c-9d1f-4557a27a511e'      
    
this call will return a string  value
    
Remove a job
------------
> DELETE /jobs/jobId 

example: curl -X DELETE 'localhost:5000/jobs/1dbccc90-4694-4ba5-b34f-4134cbd5c0c9'     

TODO Items
==========

- There is not a lot of checking on the incorrect input parameters
- There is very basic error handling, given time I would review the code and put code to handle errors more gracefully
- The process to remove stale (more than an hour old) jobs is not optimal, given time I would replace it with more logic in **addJob** function and less frequent garbage collection of jobs with stale data.
- API has not been tested comprehensively
    

