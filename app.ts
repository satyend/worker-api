import express, { json, Request, Response } from 'express';
import { config } from 'dotenv';

const app = express();
app.use(json());

config();

if(!process.env.PORT) {
    process.exit(1);
}

import { jobRouter, removeOldJobs } from './src/jobRouter';
import { AddressInfo } from 'net';

app.use('/jobs', jobRouter);

app.get('/', (req: Request, resp: Response)  => {
    resp.status(200).send('Hello world');
});

// runs every second and removes jobs that are older than an hour
setInterval(removeOldJobs, 1000);

const server = app.listen(parseInt(process.env.PORT), 'localhost', () => {
    const  { port, address } = server.address() as AddressInfo;
    console.log(`server running at ${port}, on ${address}`);
})




