import express, { Request, Response } from 'express';

import { v4 as uuidv4 } from 'uuid';
import  { Worker } from 'worker_threads';

const jobRouter = express.Router();

// jobs will contain records of the following shape
// { jobId: number, jobUrl: string, jobStatus: string, timestamp: number, contents: string, worker }

type TJobStatus = 'pending' | 'done' | 'error' ;

interface IJob {
    jobId: string;
    jobUrl: string;
    timeStamp: number; 
    contents: string, 
    jobStatus: TJobStatus;
    worker: Worker;
}

const jobs = new Map<string, IJob> ();

jobRouter.get('/', (req: Request, resp: Response) => {
    resp.status(200).send('hello from jobRouter');
});

// helper function
const addJob= (jobUrl: string) => {
    // search if the job is present in the joblist
    for(const entry of jobs.entries()) {
        const job = entry[1];
        if(jobUrl == job.jobUrl) { // we already have this job, just return id of the job
            return entry[0];
        }
    }

    // if we get here the job is not curently in the map, add it to the map
    const jobId = uuidv4();
    const job: IJob = {
        jobId, 
        jobUrl, 
        timeStamp: Date.now(), 
        contents: '', 
        jobStatus: 'pending',
        worker: new Worker('./src/urlFetcher'), // start a new worker thread
    };
    jobs.set(jobId, job);

    console.log(`added job - ${JSON.stringify(job)}`);

    job.worker.postMessage({ jobUrl, jobId });

    // update job once we hear back from the worker
    job.worker.on('message', (result) => {
        job.jobStatus = 'done';
        job.contents = result.contents;
    });

    // update job once we hear back from the worker
    job.worker.on('error', (err) => {
        job.jobStatus = 'error';
        job.contents = err.toString();
    });
    return jobId;
}

// add a job
jobRouter.post('/', (req, resp) => {
    const { jobUrl } = req.body;
    const jobId = addJob(jobUrl);

    // respond to caller
    resp.status(200).send(jobId);
});


// handle batch jobs
jobRouter.post('/batchjob', (req, resp) => {
    const { jobUrls } = req.body;
    console.log(`got batch urls - ${JSON.stringify(jobUrls)}, length = ${jobUrls.length}`);
    let ret: string[] = [];
    jobUrls.forEach((jobUrl: string, idx: number) => {
        console.log(`adding job @ ${idx}, for url - ${jobUrl}`);
        ret[idx] = addJob(jobUrl);
    })
    resp.status(200).send(ret);
});

// get job status
jobRouter.get('/:jobId', (req, resp) => {
    const { jobId } = req.params

    // find the  job in the map
    if(!jobs.has(jobId)) {
        resp.status(404).send('Bad jobId');
        return;
    }
    
    // if jobs exists return status
    resp.status(200).send(jobs.get(jobId)?.jobStatus);
});

// get job contents
jobRouter.get('/contents/:jobId', (req, resp) => {
    const { jobId } = req.params

    // find the  job in the map
    if(!jobs.has(jobId)) {
        resp.status(404).send('Bad jobId');
        return;
    }
    
    // if jobs exists and contents have been set, return contents
    const job = jobs.get(jobId);
    if(job?.jobStatus === 'done' || job?.jobStatus === 'error') {
        resp.status(200).send(job.contents);
    } else {
        resp.status(404).send('job is not done yet');
    }
});


// remove a job
jobRouter.delete('/:jobId', (req, resp) => {
    const { jobId } = req.params;

    // find the  job in the map
    if(!jobs.has(jobId)) {
        resp.status(404).send('Bad jobId');
        return;
    }

    // if job exists remove it 
    jobs.delete(jobId);
    resp.status(200).send(`Job - ${jobId} removed`);
});

const removeOldJobs = () => {
    // filter out jobs that are more than an hour old
    const expired = Date.now() - parseInt(process.env.EXPIRES_IN as string);

    for(const job of jobs.values()) {
        if(job.timeStamp <= expired) {
            console.log(`Removing old job - ${job.jobId}`);
            jobs.delete(job.jobId);
        }
    }
}

export { jobRouter, removeOldJobs };


