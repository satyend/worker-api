import { parentPort } from "worker_threads";
import fetch from 'node-fetch';

parentPort!.on('message', async (jobDescr) => {
    const { jobId, jobUrl } = jobDescr;
    try {
        const resp = await fetch(jobUrl);
        const contents = await resp.text();
        parentPort!.postMessage({ jobId, contents });
    } catch(err : any) {
        throw new Error(`error fetching url \"${jobUrl}\" : ` + err.message);
    }
})